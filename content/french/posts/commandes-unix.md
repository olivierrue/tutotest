---
title: Les commandes Unix
language: french
authors: 
- Cédric Midoux, Hélène Chiapello, Olivier Rué
categories: 
- Documentation
tags:
- unix
level: facile
date: 2020-04-08
publishdate: 2020-04-08
draft: true
type: post
thumbnail: unix.png
w3codecolor: true
#comments: true
#bibliography: biblio.bib
#nocite: '@*'
---


Ce post a pour objectif de vous présenter l'organisation du serveur migale.

***

# Introduction

## Système d'exploitation

En informatique, un système d'exploitation (souvent appelé OS - de l'anglais Operating System) est un ensemble de programmes qui dirige l'utilisation des ressources d'un ordinateur par des logiciels applicatifs. Il existe sur le marché des dizaines de systèmes d'exploitation différents, très souvent livrés avec l'appareil informatique (ordinateur, téléphone, serveur...). C'est le cas de Windows, Mac OS, Irix, Symbian OS, GNU/Linux, (pour lequel il existe de nombreuses distributions) ou Android.

Pour faire de la bioinformatique, vous serez confrontés dans la majorité des cas à des outils ou à des serveurs reposant sur des distributions Linux. Les plus fréquentes sont Ubuntu, RedHat, Debian ou CentOS. Mais pas de panique, la façon d'intéragir avec le système reste la même : par le biais de commandes Unix qu'il faudra taper dans un terminal. En effet, vous n'aurez souvent pas la possibilité d'avoir une interface graphique pour vous faciliter le travail.

Les systèmes d'exploitation de type UNIX offrent à leurs utilisateurs des centaines de commandes qui font de la console un outil pratique et extrêmement puissant. Encore faut-il savoir les utiliser !

Vous allez devoir apprendre à vous déplacer dans un arborescence de fichier, à manipuler des fichiers, à lancer des commandes, à vous connecter sur des serveurs, à y copier des fichiers... Vous ne pourrez plus vous en passer !

## Shell

Un shell Unix est un interpréteur de commandes destiné aux systèmes d'exploitation Unix et de type Unix qui permet d'accéder aux fonctionnalités internes du système d'exploitation. Il se présente sous la forme d'une interface en ligne de commande accessible depuis la console ou un terminal. L'utilisateur lance des commandes sous forme d'une entrée texte exécutée ensuite par le shell. Aujourd'hui, le shell le plus répandu est `bash`.

# Arborescence de fichiers

Le stockage des répertoires et fichiers peut être comparé à un arbre : en partant de la racine d’un arbre, en déplaçant votre doigt tout le long de l’arbre, en suivant le tronc puis les branches, vous pouvez toucher n’importe quelle feuille de cet arbre.
La racine, symbolisée et accessible par `/` représente la base du stockage des fichiers. Puis, cette base se sépare (comme des branches d’un arbre) logiquement en répertoires (dossiers), eux-mêmes séparés en sous-répertoires et sous-sous-répertoires, etc. dans lesquels sont enregistrés vos fichiers (symboliquement, les feuilles de l’arbre).

Exemple :

<div class="mermaid" style="text-align:center">
graph TD
    A["/"]
    A-->B["documents"]
    B-->D["jeux"]
    B-->C["images"]
    C-->G["image.png"]
    G:::file
    C-->H["image2.png"]
    H:::file
    B-->E["videos"]
    E-->F["film.avi"]
    F:::file
    A-->b["travail"]
    b-->c["cv"]
    c-->z["photo.png"]
    z:::file
    c-->x["cv.pdf"]
    x:::file
    A-->d["personnel"]
    d-->e["rib.jpeg"]
    e:::file
    classDef file fill:#f96;
    
</div>
<div style="text-align:center"><i>Figure 1 : Exemple d'une arborescence de type Unix</i></div>

Les fichiers sont représentés en orange, les répertoires en violet.

Pour accéder à un fichier, la façon la plus simple est de partir de la racine et de suivre le chemin vers le fichier.

Exemple d'accès au fichier *image2.png*:

<div class="mermaid" style="text-align:center">
graph LR
    A["/"]
    A-->B["documents"]
    B-->C["images"]
    C-->H["image2.png"]
    H:::file
    classDef file fill:#f96;
</div>

En remplaçant les flèches par des `/`, voici comment on accède au fichier *image2.png*

<pre class="bash"><code class="hljs lua">
/documents/images/image2.png
</code>
</pre>

C'est ce qu'on appelle un chemin **absolu**, c'est-à-dire qu'il démarre de la racine.

## Le répertoire courant

Mais il y a d'autres façons d'accéder à un fichier. On peut y accéder par des chemins dits **relatifs**, c'est-à-dire relativement à notre emplacement dans l'arborescence. Si nous sommes dans le répertoire *images*, nous n'avons pas besoin de réécrire toute l'arborescence pour accéder au fichier *image2.png*. Il est donc essentiel de savoir à quel endroit nous nous trouvons dans l'arborescence. Pour cela, il faut utiliser la commande `pwd` (print name of current/working directory).

<pre class="bash"><code class="hljs lua">
$ pwd
/documents/images
</code>
</pre>

## Le répertoire utilisateur

Le système définit un répertoire utilisateur. C'est un répertoire comme un autre mais qui est accessible via le raccourci `~`. Lors d'une connexion à un serveur distant, c'est dans ce répertoire que vous atterissez s'il est défini comme tel.

Dans notre exemple, si le répertoire utilisateur est *personnel*, alors je peux accéder au fichier rib.jpeg avec ce chemin :

<pre class="bash"><code class="hljs lua">
~/rib.jpeg
</code>
</pre>

qui est équivalent bien sûr à :

<pre class="bash"><code class="hljs lua">
/personnel/rib.jpeg
</code>
</pre>

## Changer de répertoire

Le répertoire courant est symbolisé par `.`.


# Généralités sur les commandes Unix

La plupart des commandes acceptent des arguments. On peut distinguer deux types d'arguments : 

* les options, qui sont en général précédées du signe `-` pour leur forme courte ou `--` pour leur forme longue, plus verbeuse.

<pre class="bash"><code class="hljs lua">
ls -l --color
</code>
</pre>

Cette commande affiche le contenu du répertoire courant de façon détaillée (-l) et en colorant les fichiers selon leur type (--color). 

* les chaînes de caractères, qui peuvent représenter un fichier, une expression régulière ou une commande interne.

<pre class="bash"><code class="hljs lua">
grep toto mon_fichier.txt
</code>
</pre>

Cette commande recherche la chaîne titi dans le fichier mon_fichier.txt. 



# Quizz

<div id="quiz">
  <div id="quiz-header">
    <p class="faded">Êtes-vous prêt à utiliser Migale ?</p>
  </div>
  <div id="quiz-start-screen">
    <p><a href="#" id="quiz-start-btn" class="quiz-button">Start</a></p>
  </div>
</div>




